# Descrição em português #

Codigo C de um Simulador de Memoria Cache e Memoria Principal

Disciplina: Organizacao de Computadores Digitais I

Professor: Paulo Sergio Lopes de Souza

Aluno PAE: Carlos Emilio de Andrade Cacho

Grupo 11 - Alunos:

* Caio César Almeida Guimarães                        8551497
* Helder de Melo Mendes                               8504351
* Henrique Cintra Miranda de Souza Aranha             8551434
* Lucas Eduardo Carreiro de Mello                     8504218

# English description #

Cache memory and RAM simulator written in C language

Course: Computer Organization

Professor: Paulo Sergio Lopes de Souza

Supporting Student: Carlos Emilio de Andrade Cacho

Grupo 11 - Students:

* Caio César Almeida Guimarães                        8551497
* Helder de Melo Mendes                               8504351
* Henrique Cintra Miranda de Souza Aranha             8551434
* Lucas Eduardo Carreiro de Mello                     8504218