/*===========================================================================
|       Trabalho 2
|
|       Codigo C de um Simulador de Memoria Cache e Memoria Principal
|============================================================================
|
|   Disciplina: Organizacao de Computadores Digitais I
|   Professor: Paulo Sergio Lopes de Souza
|   Aluno PAE: Carlos Emilio de Andrade Cacho
|
|   Grupo 11
|   Alunos:
|           Caio César Almeida Guimarães                        8551497
|           Helder de Melo Mendes                               8504351
|           Henrique Cintra Miranda de Souza Aranha             8551434
|           Lucas Eduardo Carreiro de Mello                     8504218
\===========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define DIGITS_NUM(a) ( (int)ceil( log10(a) ) )
#define INT_MAX_DIGITS DIGITS_NUM(INT_MAX)
#define LRU 0
#define LFU 1
#define WriteThrough 0
#define WriteBack 1

struct ST_Auxiliar { //Guarda variáveis que ajudam na geração da saída.
    int num_access;
    int hit_rate;
    char *fmtStr[9]; //Vetor de strings de formatação.
    char subPolicy[4];
} Auxiliar;

typedef struct ST_Block {

    char update;
    char validity;
    char *data;
    int substitution; //Bits de controle de política de substituição.
    int tag;

} Block;

typedef struct ST_Set {

    Block *block;

} Set;

struct ST_cache_memory { //Estrutura que representa a Memória Cache.

    //Atributos de configuração.
    char substitutionType; //Se 0 a política é LRU e 1 a política é LFU.
    char updateType; //Se 0 a política de atualização é Write Through e se 1 é Write Back.

    int associativity;
    int blockNum; //Quantidade de blocos.
    int blockSize; //Tamanho de cada bloco em palavras.
    int setNum; //Número de conjuntos.

    Set *set;

} Cache;

struct ST_ram { //Estrutura que respresenta a RAM.

    int size; //Tamanho da memória em bytes.
    char *data;

} RAM;

// Carrega a configuração, inicializa os parametros da cache e a RAM.
int load_config();

// Confere se o número recebido é positivo e multiplo de potência de 2.
int isPositivePower2(int number);

// Acessa a memoria cache, e se der falta acessa a memoria principal.
int memory_access(int type, int addr_a_byte, char *data);

// Le um dado da memoria cache.
int cache_read(int addr_a_byte, char *data);

// Le um bloco da memoria principal.
void ram_read(int addr_a_byte, char *data);

// Escreve um dado na memoria cache.
int cache_write(int addr_a_byte, char *data);

// Escreve um dado na memoria principal.
void ram_write(int addr_a_byte, char *data);

// Função que imprime uma representação gráfica da RAM.
void ram_print();

// Função que imprime uma representação gráfica da Cache.
void cache_print();

// Funcao que converte um inteiro em 4 bytes/chars
void int_to_chars(int int_num, char *char_num);

// Cria uma string que representa o número fornecido em binário.
char *int_to_binary_str(int num);

// Cria as strings de formatação.
void create_format_strings();

// Libera a memória alocada dinamicamente para a struct auxiliar.
void auxiliar_free();

// Libera a memória alocada dinamicamente para a struct auxiliar.
void cache_free();

// Libera a memória alocada dinamicamente para a struct auxiliar.
void ram_free();

int main(int argc, char *argv[])
{
    int type; // Armazena o tipo de acesso
    int addr_a_byte; // Armazena o endereco
    int aux_data; // Armazena o dado a ser escrito como um inteiro

    char *data = malloc(4 * sizeof(char)); // Armazena o dado a ser escrito transformado em 4 chars ou o dado lido

    // Le o arquivo de configuracoes
    while(load_config() == -1) // Se houver erros no arquivo
    {
        printf("\nO arquivo de configuracoes esta errado, modifique-o e aperte enter\n");
        getchar();
    }

    // Inicializa os valores da struct
    create_format_strings();

    // Loop principal
    while(1)
    {
        if(scanf("%d", &type) == 1){ // Le o tipo de acesso
            if(type == -1) // Se for -1 o programa é fechado
                break;

            scanf("%d", &addr_a_byte); // Le o endereco

            if(type == 1) // Se for uma escrita
            {
                scanf("%d", &aux_data); // Le o dado a ser escrito
                int_to_chars(aux_data, data); // Transforma aux_data em 4 bytes para *data
            }

            if ( memory_access(type, addr_a_byte, data) != -1 )
            { //Se não ocorreu erro na operação.
                printf("Sucesso!\n");
                if ( type == 1) {
                printf("Dado lido: %d\n",
                            data[3] & 0x000000FF | //Formação da palavra para impressão.
                            (data[2] << 8 ) |
                            (data[1] << 16 ) |
                            (data[0] << 24)
                        );
                }
            }
        }else{
            printf("Entrada Inválida\n");
        }
    }

    printf("Acessos: %d\nAcertos: %d\nFaltas: %d\n\n",
                Auxiliar.num_access,
                Auxiliar.hit_rate,
                Auxiliar.num_access - Auxiliar.hit_rate
            );
    //Impressão do estado final da ram e cache.
    ram_print();
    cache_print();

    free(data);
    auxiliar_free();
    cache_free();
    ram_free();

    return 0;
}

void ram_read(int addr_a_byte, char *data) {
    int i,
        end = Cache.blockSize * 4;

    for ( i = 0; i < end; i++ )
        data[i] = RAM.data[addr_a_byte + i];
}

void ram_write(int addr_a_byte, char *data) {
    int i,
        end = Cache.blockSize * 4;

    for ( i = 0; i < end; i++ )
        RAM.data[addr_a_byte + i] = data[i];
}

FILE* open_file() {
    FILE* file;
    file = fopen("config.cfg", "r");

    if( file == NULL ) {
        printf("Erro ao carregar arquivo!\n");
    }
    return file;
}

int isPowerOfTwo(unsigned int number){
    return ( (number & (~number + 1)) == number );
}

int load_config() {

    int i, j, aux;
    char charBuff[10];  //Buffer para verificação dos tipos de politica lidos pelo console

    FILE* file = open_file();

    if(file != NULL) {

        //Leitura e armazenamento dos dados do arquivo de configuração
        fscanf(file, "qtde total de bytes na RAM: %d\n", &RAM.size);
        if (RAM.size > 0){
            if(isPowerOfTwo(RAM.size) == -1){
                printf("ERRO: Toatl de bytes na RAM não é potência de 2!\n");
            }
        }else{
            printf("ERRO: Total de bytes na RAM não positivo!\n");
        }

        fscanf(file, "qtde de palavras por bloco na cache: %d\n", &Cache.blockSize);
        if (Cache.blockSize > 0){
            if(isPowerOfTwo(Cache.blockSize) == -1){
                printf("ERRO: Palavras por bloco na Cache não é potência de 2!\n");
            }
        }else{
            printf("ERRO: Palavras por bloco na Cache não positivo!\n");

        }

        fscanf(file, "qtde de blocos na cache: %d\n", &Cache.blockNum);
        if (Cache.blockNum > 0){
            if(isPowerOfTwo(Cache.blockNum) == -1){
                printf("ERRO: Nº de blocos na Cache não é potência de 2!\n");
            }
        }else{
            printf("ERRO: Nº de blocos na Cache não positivo!\n");
        }

        fscanf(file, "grau de associatividade: %d\n", &Cache.associativity);
        if (Cache.associativity > 0){
            if(isPowerOfTwo(Cache.associativity) == -1){
                printf("ERRO: Associatividade não é potência de 2!\n");
            }
        }else{
            printf("ERRO: Associatividade não positivo!\n");
        }
        if ( (Cache.associativity < 1) || (Cache.associativity > Cache.blockNum) ){
            printf("ERRO: Grau de associatividade inválido! (Deve ser como 1 <= associatividade <= 'blocos na cache')\n");
            return (-1);
        }

        //Verificação de erros na política de substituição.
        fscanf(file, "política de substituição: %s\n", charBuff);
        if (strcmp(charBuff, "LRU") == 0){
            Cache.substitutionType = LRU;
            strcpy (Auxiliar.subPolicy,"LRU");
        }else{
            if (strcmp(charBuff, "LFU") == 0){
                Cache.substitutionType = LFU;
                strcpy (Auxiliar.subPolicy,"LFU");
            }else{
                printf("ERRO: Política de substituição não aceita! (Entrada deve ser 'LRU' ou 'LFU')\n");
                fclose(file);
                return (-1);
            }
        }

        //Verificação de erros na política de atualização.
        fscanf(file, "política de atualização: write %s", charBuff);
        if (strcmp(charBuff, "back") == 0){
            Cache.updateType = WriteBack;
        }else{
            if (strcmp(charBuff, "through") == 0){
                Cache.updateType = WriteThrough;
            }else{
                printf("ERRO: Política de atualização não aceita! (Entrada deve ser 'write back' ou 'write through')\n");
                fclose(file);
                return (-1);
            }
        }

        //Inicialização da memória Cache.
        Cache.setNum = (Cache.blockNum/Cache.associativity);
        Cache.set = (Set*)malloc(Cache.setNum*sizeof(Set)); //Alocação dos conjuntos da Cache.

        for ( i = 0; i < Cache.setNum; i++ ) {
        Cache.set[i].block = ( Block* ) malloc( Cache.associativity*sizeof(Block) );
            for( j = 0; j < Cache.associativity; j++ ) {
                Cache.set[i].block[j].validity = 0;
                Cache.set[i].block[j].data = (char*) malloc(4*Cache.blockSize*sizeof(char) );
            }
        }

        //Inicialização da memória RAM.
        RAM.data = (char *) malloc( sizeof(char) * RAM.size );
        for ( i = 0; i < RAM.size; i += 4 ) { //Inicializa os dados na RAM com o endereço * 10.
            int_to_chars( i * 10, &RAM.data[i] );
        }

    }else{
        printf("ERRO: Leitura do arquivo! (Confira se o arquivo 'config.cfg' está neste diretório)\n");
        return (-1);
    }

    fclose(file);
    return 1;
}

// Acessa a memoria cache, e se der falta acessa a memoria principal
int memory_access(int type, int addr_a_byte, char *data)
{

    // Se o endereco esta fora dos limites da memoria
    if(addr_a_byte < 0 || addr_a_byte >= RAM.size)
    {
        printf("ERRO: O endereco solicitado esta fora dos limites da memoria RAM.\n");
        return -1;
    }

    // Se nao for o endereco de uma palavra
    if(addr_a_byte % 4 != 0)
    {
        printf("ERRO: O valor nao corresponde a um endereco de uma palavra de 32 bits pois nao e multiplo de 4.\n");
        return -1;
    }

    // Se o acesso for de leitura
    if(type == 0)
    {
        Auxiliar.num_access++; // Ocorreu um acesso

        // Tenta ler o dado na mem. cache
        if(cache_read(addr_a_byte, data) != -1) // Retornar algo diferente de -1 indica que houve acerto
            Auxiliar.hit_rate++; // Ocorreu um acerto
    }

    // Se o acesso for de escrita
    else if(type == 1)
    {
        Auxiliar.num_access++; // Ocorreu um acesso

        // Escreve o dado na mem. cache
        if(cache_write(addr_a_byte, data) != -1){ // Retornar algo diferente de -1 indica que houve acerto
            Auxiliar.hit_rate++; // Ocorreu um acerto
        }
    }

    // Se o acesso for um tipo nao esperado
    else
    {
        printf("ERRO: Tipo de acesso nao existe. 0 -> leitura, 1 -> escrita.\n");
        return -1;
    }

    return 1;
}

char *int_to_binary_str(int num) {
    int i, strSize = 2 + ( num == 0 ? 0 : (int) floor( log2(num) ) );
    char *binaryNum = (char *) malloc( sizeof(char) * strSize );

    binaryNum[--strSize] = '\0';
    while ( strSize > 0 ) {
        binaryNum[--strSize] = (num % 2 == 1) ? '1' : '0';
        num /= 2;
    }
    return binaryNum;
}

char *allocate_str(size_t size) {
    return (char *) malloc( sizeof(char) * ++size );
}

void create_format_strings() {
    char strBuffer[20 + 2 * INT_MAX_DIGITS];
    int blockSizeDigits = DIGITS_NUM(Cache.blockSize),
        blockNumDigits = DIGITS_NUM(Cache.blockNum),
        addressSize = log2(RAM.size), //Tamanho do endereço em bits.
        setSize = log2(Cache.setNum), //Tamanho em bits de SET.
        tagSize = addressSize - setSize - log2(Cache.blockSize) - 2, //Tamanho em bits de size.
        tagShift = addressSize - tagSize,
        ramSize = RAM.size/4/Cache.blockSize, //Tamanho da RAM em blocos.
        ramMaxAddres = RAM.size - 4,
        ramMaxWord = ramMaxAddres/4,
        ramAddresDigits = DIGITS_NUM(ramMaxAddres),
        ramWordDigits = DIGITS_NUM(ramMaxWord),
        ramBlockDigits = DIGITS_NUM(ramMaxWord/Cache.blockSize),
        maxDataDigits = INT_MAX_DIGITS,
        ramFmtSize = DIGITS_NUM(ramBlockDigits) + DIGITS_NUM(ramWordDigits) +
                            DIGITS_NUM(ramAddresDigits) + DIGITS_NUM(maxDataDigits),
        updateDigits = 0;

    //Ajusta os valores dos dígitos para manter a tabela alinhada.
    ramBlockDigits = (ramBlockDigits > 5) ? ramBlockDigits : 5; //5 é o tamanho da palavra Bloco.
    ramWordDigits = (ramWordDigits > 7) ? ramWordDigits : 7; //7 é o tamanho da palavra  Palavra.
    ramAddresDigits = (ramAddresDigits > 8) ? ramAddresDigits : 8; //8 é o tamanho da palavra Endereço.

    blockSizeDigits = (blockSizeDigits > ramWordDigits ) ? blockSizeDigits : ramWordDigits;
    setSize = DIGITS_NUM(Cache.setNum - 1); //3 é o tamanho mínimo para set.
    setSize = (setSize > 3) ? setSize : 3;
    tagSize = (tagSize > 3) ? tagSize : 3;

    //Formato do início da primeira linha.
    sprintf( strBuffer, "|%%-%ds|", setSize );
    Auxiliar.fmtStr[0] = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[0], strBuffer );

    //Formato para bit de set nas linhas subjacentes.
    sprintf( strBuffer, "|%%-%dd|", setSize );
    Auxiliar.fmtStr[1] = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[1], strBuffer );

    //Formato da segunda linha antecedendo words: V|politica de sub|TAG|politica de atual|
    sprintf( strBuffer, "V|%%-%ds|%%-%ds|", blockSizeDigits, tagSize );
    Auxiliar.fmtStr[3]  = allocate_str( strlen(strBuffer) + 3 );
    strcpy( Auxiliar.fmtStr[3] , strBuffer );

    //Formato complementar da segunda linha: Words|
    sprintf( strBuffer, "W%%-%dd|", maxDataDigits - 1 );
    Auxiliar.fmtStr[4]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[4] , strBuffer );

    //Formato das demais linhas: bit de validade|controle da pol. de substituição|TAG|controle de pol. atualização|words...
    if ( Cache.updateType == WriteBack  ) { //Se a política de atualização for Write Back
        strcat( Auxiliar.fmtStr[3] , "WB|" ); //Completa o formato da primeira linha.
        sprintf( strBuffer, "%%-1d|%%-%dd|%%-%ds|%%-2d|", blockSizeDigits, tagSize );
        updateDigits = 3;
    }
    else //Se a política de atualização for Write Through
        sprintf( strBuffer, "%%-1d|%%-%dd|%%-%ds|", blockSizeDigits, tagSize );
    Auxiliar.fmtStr[5]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[5] , strBuffer );

    //Formato para a impressão das words.
    sprintf( strBuffer, "%%-%dd|", maxDataDigits );
    Auxiliar.fmtStr[6]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[6] , strBuffer );

    //Formato para a parte de blocos da primeira linha.
                                                    //Tamanho de V + polAtSize(blockSizeDigits) + tagSize(tagSize) + Cache.blocksize * (blockSizeDigits + 1) - 3
    sprintf( strBuffer, "Bloco %%-%dd|", maxDataDigits + updateDigits + tagSize + Cache.blockSize * (blockSizeDigits + 2) - 2 );
    Auxiliar.fmtStr[2]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[2] , strBuffer );

    //Formatação da primeira linha da RAM.
    sprintf( strBuffer, "|%%-%ds|%%-%ds|%%-%ds|%%-%ds|\n",
                ramBlockDigits, ramWordDigits, ramAddresDigits, maxDataDigits
            );
    Auxiliar.fmtStr[7]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[7] , strBuffer );

    //Formatação da segunda linha da RAM.
    sprintf( strBuffer, "|%%-%dd|%%-%dd|%%-%dd|%%-%dd|\n",
                ramBlockDigits, ramWordDigits, ramAddresDigits, maxDataDigits
            );
    Auxiliar.fmtStr[8]  = allocate_str( strlen(strBuffer) );
    strcpy( Auxiliar.fmtStr[8] , strBuffer );
}

int cache_read(int addr_a_byte,char *data )
{
	int wordOffset, set, tag;
	int i, j, blockStart, smallest, index;
    int r_block_addr, //Armazenará o  cálculo do endereço de RAM, para descarregar o bloco a ser substituido na Cache.
        rTag; //Guarda tag do bloco o qual vamos descarregar na RAM.

	wordOffset=(addr_a_byte/4)%(Cache.blockSize); // Word offset recebe os bits menos significativos correposdentes ao logaritmo na base 2 do numero de palavras por bloco.
	set=(addr_a_byte/(4*Cache.blockSize))%(Cache.setNum);// O numero do conjunto recebe os bits menos significativos correspondentes ao logaritmo na base 2 do numero de conjuntos,sendo que primeiramente o endereco precisar ser deslocado para a direita de (4*numero de blocos) para que isso ocorra
	tag=(addr_a_byte/(4*Cache.blockSize*Cache.setNum));// A tag Ã© determinada pelos bits que sobraram do endereco

	for(i=0;i<Cache.associativity;i++)
	{
		if(Cache.set[set].block[i].validity!=0)// A checagem das tags é feita se o bit de validade for diferente de 0. Se for 0, ele possui lixo.
		{
			if(Cache.set[set].block[i].tag==tag)// Se a tag do endereco for igual a tag do bloco referenciado
			{
				// O bloco cujo endereco foi referenciado foi achado
				for(j=0;j<4;j++)
				{
					data[j] =Cache.set[set].block[i].data[wordOffset*4+j];
				}
				if(Cache.substitutionType==LRU)
				{
					for(j=0;j<Cache.associativity;j++)
					{
						if( (Cache.set[set].block[j].validity != 0) &&  (i != j) && (Cache.set[set].block[j].substitution != 0) && (Cache.set[set].block[i].substitution != Cache.associativity-1) )
						{
							Cache.set[set].block[j].substitution--;
						}
					}
                    Cache.set[set].block[i].substitution = Cache.associativity-1;
				}
				else if(Cache.substitutionType==LFU)
				{
					if(Cache.set[set].block[i].substitution+1 != INT_MAX)
					{
						Cache.set[set].block[i].substitution++;
					}
				}
				return 1;
			}
		}
        else { // O bloco nao foi achado na memoria cache, portanto precisa ser recuperado do nivel inferior na hierarquia de memoria, neste caso a RAM.
            // Existe bloco vazio. É preciso checar somente até o primeiro bloco disponivel (ou com bit de validade igual a 0).
            Cache.set[set].block[i].tag = tag;
            Cache.set[set].block[i].update = 0;
            Cache.set[set].block[i].validity = 1;

            if(Cache.substitutionType == LRU)
            {
                for(j=0; j<i; j++)// Nesse caso precisamos iterar somente até o primeiro bloco que possuia bit de validade '0'.
                {
                    if( (Cache.set[set].block[j].substitution != 0) && (Cache.set[set].block[i].substitution != Cache.associativity-1) )
                    {
                        Cache.set[set].block[j].substitution--;
                    }
                }
                Cache.set[set].block[i].substitution = Cache.associativity-1;
            }
            else
                if(Cache.substitutionType == LFU)
                {
                    Cache.set[set].block[i].substitution = 1;
                }

            blockStart = addr_a_byte - (4*wordOffset); // Cálculo do início do bloco na RAM.
            for(j=0; j<4*Cache.blockSize; j++)// Transfere o bloco inteiro para a Cache.
            {
                Cache.set[set].block[i].data[j] = RAM.data[blockStart+j];//transfere os dados da RAM para a cache
            }
            // A transferência de dados é realizada da cache para o processador
            for(j=0; j<4; j++)
            {
                data[j] = Cache.set[set].block[i].data[wordOffset*4+j];
            }
            return -1;
        }
	}
	// Cache cheia. Se nenhum bloco está disponivel, precisamos substituir um dos blocos segundo a politica de substituição.
    // Seja LFU ou LRU, o bloco que é substituido é aquele com o menor valor de bits de substituição.
	index = 0; // Indicará qual bloco será substituido.
	if (Cache.substitutionType == LRU)
	{
		smallest = Cache.associativity;//uma unidade acima do maior valor atribuivel em LRU
	}
	else
        if (Cache.substitutionType == LFU)
    	{
    		smallest = INT_MAX;//uma unidade acima do maior valor atribuivel em LFU
    	}
	for(i=0; i<Cache.associativity; i++)
	{
		if (Cache.set[set].block[i].validity !=0 &&  Cache.set[set].block[i].substitution < smallest)
		{
			smallest = Cache.set[set].block[i].substitution;
			index = i;
		}
	}

    //Tenho o bloco a ser substituido em 'index'.
    //Este cálculo só será executado caso o bloco destinado a leitura não fora encontrado anteriormente.
    //Desta forma é necessário carregar o que está na RAM, para depois le-lo.

    // Se for "Write-Back" devo descarregar o conteúdo na RAM antes de escrever no bloco.
    if (Cache.updateType == WriteBack){
        if ( (Cache.set[set].block[index].update) == 1 ){ //Verifica bit de modificação
            rTag = Cache.set[set].block[index].tag;
            r_block_addr = (rTag*Cache.setNum*Cache.blockSize*4); //Cálculo de recuperação do endereço do bloco atual.
            ram_write(r_block_addr, Cache.set[set].block[index].data); //Descarrega o bloco atual da Cache para a RAM.
            Cache.set[set].block[index].update = 0; //Altera bit 'M' para 0.
        }
    }

    //Carregar bloco para leitura da RAM.
    char* readBlock = (char*)malloc((Cache.blockSize*4)*sizeof(char));
    ram_read(addr_a_byte, readBlock);

    //Escrever o bloco lido da RAM na Cache.
    for (i = 0; i < (Cache.blockSize*4); i++){
        Cache.set[set].block[index].data[i] = readBlock[i];
    }
	//Substituicao do bloco
	Cache.set[set].block[index].tag = tag;
	Cache.set[set].block[index].update = 0;

	if(Cache.substitutionType == LRU)
	{
		for(j=0; j<Cache.associativity; j++)// Nesse caso precisamos iterar somente até o primeiro bloco em que o bit de validade era 0.
		{
			if( (Cache.set[set].block[j].validity != 0)  && (index != j) && (Cache.set[set].block[j].substitution != 0) && (Cache.set[set].block[index].substitution != Cache.associativity-1) )
			{
				Cache.set[set].block[j].substitution--;
			}
		}
        Cache.set[set].block[index].substitution = Cache.associativity-1;
	}else
        if(Cache.substitutionType==LFU)
    	{
    		Cache.set[set].block[index].substitution=1;
    	}
	Cache.set[set].block[index].validity = 1;

	// A transferencia de dados é feita
	for(j=0;j<4;j++)
	{
		data[j] = Cache.set[set].block[index].data[wordOffset*4+j];
	}
	return -1;// O retorno é -1 pois uma falta deve ser reportada
}


int cache_write(int addr_a_byte, char *data){

    addr_a_byte = addr_a_byte/4; //Elimina o Byte Offset do endereço
    int nWord = (addr_a_byte % (Cache.blockSize)); //A word do endereço é extraida tomando-se o resto da divisão pelo nº de palavras por bloco

    addr_a_byte = addr_a_byte/Cache.blockSize; //Elimina o Word Offset do endereço.
    int ramBlock = addr_a_byte; //O resultado é o nº do bloco na RAM

    int nSet = (addr_a_byte % (Cache.setNum)); //O nº do conjunto é obtido pelo resto da divisão pelo nº de conjuntos da Cache.
    int nTag = addr_a_byte/Cache.setNum; //Elimina o Set do endereço, o resultado é o valor da Tag.

    //Cálculo do endereço de início do bloco para escrita na RAM.
    int ram_block_addr = ramBlock*Cache.blockSize*4; //Obtem-se a palavra na RAM pelo produto com o nº de palavras por bloco. O produto por "4" acresenta o Byte Offset.
    int r_block_addr, //Endereço recuperado do bloco atualmente na Cache.
        rTag; //Guarda tag do bloco o qual vamos recuperar o endereço para escrita na RAM.

    //============== Procura pelo bloco na Cache ==============

    //Procura pelo endereço referido comparando 'nTag' com as Tags do conjunto 'nSet'.
    //Caso o bit de validade esteja ativo e ocorra acerto na comparação, uma flag e o nº do bloco no qual a Tag foi encontrada são marcados.
    int i, found = 0, blockFound, blockAvaiable = 0, smallest;
    int fault = 0; //Armazenara '0' caso o bloco a ser escrito estava na Cache, '1' caso foi preciso consultar a RAM(ocorreu falta).
    for (i = 0; i < Cache.associativity; i++){

        if( (Cache.set[nSet].block[i].validity == 1) && (Cache.set[nSet].block[i].tag == nTag) ){
            blockAvaiable = i; //Guardo bloco de ocorrência da Tag
            found = 1;
        }
    }

    //============== BLOCO NÃO ENCONTRADO ==============

    //Caso a Tag nÃ£o seja encontrada partiremos para outras verificações a fim de localizar um bloco ideal para escrita.
    //Caso a Tag seja encontrada partiremos para a escrita de 'data' na word adequada do bloco.
    if (found == 0){ //Se não há referências na Cache para esta Tag.

        fault = 1; //Ocorreu falta;
        //============== Verifica se a Cache esta cheia ==============
        int cacheFull = 1; //Assume em início que o conjunto esteja cheio
        for (i = 0; i < Cache.associativity; i++){

            if( (Cache.set[nSet].block[i].validity == 0) && (cacheFull == 1) ){ //Verifica se o conjunto está cheio
                blockAvaiable = i;
                cacheFull = 0;
            }
        }

        //Caso o conjunto esteja cheio, verificaremos nossas polÃ­ticas de substituição para saber qual bloco será substituido.
        //Caso contrário, escreveremos o conteúdo de 'data' no primeiro bloco vazio encontrado.
        //Ao fim deste "if", 'blockAvaiable' deverá referenciar o bloco no qual escreveremos 'data', independentemente da situação anterior do conjunto.
        if (cacheFull == 1){
        //============== CACHE LOTADA ==============

            //============== Verifica qual a política de substituição ==============
            //Selecionar o bloco com a menor contagem em 'substitution' e o guarda em 'smallest'.

            //============== 'LRU' ==============
            if(Cache.substitutionType == LRU){
                smallest = Cache.associativity;
            }else{
            //============== 'LFU' ==============
                if(Cache.substitutionType == LFU){
                    smallest = INT_MAX;
                }
            }

            for (i = 0; i< Cache.associativity; i++){
                if ( (Cache.set[nSet].block[i].validity) != 0 && (Cache.set[nSet].block[i].substitution < smallest) ){
                    smallest = Cache.set[nSet].block[i].substitution;
                    blockAvaiable = i;
                }
            }
            //'blockAvaiable' é o retorno das políticas de substituição

            //Caso seja "Write-Back" é necessaŕio conferir o bit de modificação do bloco da Cache, e se for '1' escrevê-lo na RAM.
            //Caso contrário ou caso o bit de modificação de "Write-Back" seja '0', iremos carregar o bloco onde queremos escrever o que esta em 'data'.
            if (Cache.updateType == WriteBack){

                if ( (Cache.set[nSet].block[blockAvaiable].update) == 1 ){ //Verifica bit de modificação
                    rTag = Cache.set[nSet].block[blockAvaiable].tag;
                    r_block_addr = (rTag*Cache.setNum*Cache.blockSize*4); //Cálculo de recuperação do endereço do bloco atual.
                    ram_write(r_block_addr, Cache.set[nSet].block[blockAvaiable].data); //Descarrega o bloco atual da Cache para a RAM.
                    Cache.set[nSet].block[blockAvaiable].update = 0; //Altera bit 'M' para 0.
                }
            }
        }
        //============== CACHE POSSUI ESPAÇO ==============
        //Primeiro bloco vazio ou o substituivel está em 'blockAvaiable'
    }

    //Esta condicional só será executada caso o bloco destinado a escrita não fora encontrado anteriormente.
    //Desta forma é necessário carregar o que está na RAM, para depois escrever 'data'.
    if (fault == 1){

        //Carregar bloco do endereço da RAM.
        char* readBlock = (char*)malloc((Cache.blockSize*4)*sizeof(char));
        ram_read(ram_block_addr, readBlock);

        //Escrever o bloco lido da RAM na Cache.
        for (i = 0; i < (Cache.blockSize*4); i++){
            Cache.set[nSet].block[blockAvaiable].tag = nTag;
            Cache.set[nSet].block[blockAvaiable].data[i] = readBlock[i];
        }
    }

    //============== BLOCO ENCONTRADO (OU ALOCADO) ==============

    //'blockAvaiable' contêm a referência ao bloco

    //Copia "byte a byte" a word armazenada em 'data' e a escreve no bloco.
    int wordPos = 4*nWord; //'wordPos' calcula a posição de início da word a ser escrita na Cache.
    for (i = wordPos; i< (wordPos+4); i++){
        Cache.set[nSet].block[blockAvaiable].data[i] = data[i-wordPos];
    }

    //============== Modifica a política de atualização ==============

    //============== "W-THROUGH" ==============

    //Caso seja "Write-Back" altera o bit de modificação para '1'.
    //Caso seja "Write-Through" escreve o bloco recem alterado na memória RAM.
    if (Cache.updateType == 0){ //'Write-Through' (updateType = 0).
        ram_write(ram_block_addr, Cache.set[nSet].block[blockAvaiable].data); //Descarrega o bloco na RAM.

    }else{
    //============== "W-BACK" ==============

        Cache.set[nSet].block[blockAvaiable].update = 1; //Altera bit de modificação para 1.
    }

    //============== Modifica a política de substituição ==============

    //============== 'LRU' ==============
    if(Cache.substitutionType == LRU){
        for (i = 0; i < Cache.associativity; i++){ //Reduz a contagem para todos os outros
            if ( (Cache.set[nSet].block[i].validity) != 0 && (i!=blockAvaiable) && (Cache.set[nSet].block[i].substitution != 0) && (Cache.set[nSet].block[blockAvaiable].substitution != (Cache.associativity-1) ) ){
                Cache.set[nSet].block[i].substitution--;
            }
        }
        Cache.set[nSet].block[blockAvaiable].substitution = (Cache.associativity - 1); //Marca como o mais recentemente utilizado
    }else{
    //============== 'LFU' ==============
        if ( (Cache.substitutionType == LFU) && (fault == 0) ) { //Se o bloco estava na Cache, aumenta a contagem de frequência dele.
            if ( Cache.set[nSet].block[blockAvaiable].substitution+1 != INT_MAX ){
                Cache.set[nSet].block[blockAvaiable].substitution++;
            }
        }else{ //Se eu carreguei ele da RAM, reseto a contagem para '1'.
            Cache.set[nSet].block[blockAvaiable].substitution = 1;
        }
    }

    //Em qualquer das alternativas, tanto escrevendo em um bloco novo como pegando um da RAM, o bloco em que escreve-mos passa a ser válida(caso já não fosse).
    Cache.set[nSet].block[blockAvaiable].validity = 1;

    //Retorno da função para análise de faltas na 'memory_acess'.
    if (fault == 0){
        return 1;
    }else{
        return (-1); }
}

void cache_print() {

    int i, j, k, aux;

    //Gera a primeira linha da tabela.
    printf(Auxiliar.fmtStr[0], "SET");
    for ( i = 0; i < Cache.associativity; i++ ) //Impressão de Bloco 0|Bloco 1 ...
        printf(Auxiliar.fmtStr[2] , i);
    printf("\n");

    //Gera a segunda linha da tabela.
    printf(Auxiliar.fmtStr[0], "");
    for ( i = 0; i < Cache.associativity; i++ ) { //Impressão de V|LRU\LFU|TAG...
        printf(Auxiliar.fmtStr[3] , Auxiliar.subPolicy, "TAG");
        for ( j = 0; j < Cache.blockSize; j++ ) //Impressão de W0|W1...
            printf(Auxiliar.fmtStr[4] , j);
    }

    char *binStr;
    for ( i = 0; i < Cache.setNum; i++ ) { //Imprime informação sobre um conjunto.
        printf("\n");
        //binStr = int_to_binary_str(i);
        printf(Auxiliar.fmtStr[1], i);
        //free(binStr);

        for ( j = 0; j < Cache.associativity; j++ ) { //Imprime informação sobre cada bloco.
            binStr = int_to_binary_str( Cache.set[i].block[j].tag );
            printf(     Auxiliar.fmtStr[5] ,
                        Cache.set[i].block[j].validity,
                        Cache.set[i].block[j].substitution,
                        binStr,
                        Cache.set[i].block[j].update
                   );
            free(binStr);

            for ( k = 0; k < Cache.blockSize; k++ ) { //Imprime cada palavra do bloco.
                aux = 4 * k;
                printf( Auxiliar.fmtStr[6] ,
                         Cache.set[i].block[j].data[aux + 3] & 0x000000FF | //Formação da palavra.
                         ( Cache.set[i].block[j].data[aux + 2] << 8 ) |
                         ( Cache.set[i].block[j].data[aux + 1] << 16 ) |
                         ( Cache.set[i].block[j].data[aux] << 24)
                       );
            }
        }
    }
    printf("\n");
}

void ram_print() {

    int i;

    printf( Auxiliar.fmtStr[7] , "Bloco", "Palavra", "Endereço", "Dado" );
    for ( i = RAM.size - 4; i >= 0; i -= 4 )
        printf( Auxiliar.fmtStr[8] , i/4/Cache.blockSize, i/4, i,
                 RAM.data[i + 3] & 0x000000FF | //Formação da palavra.
                 ( RAM.data[i + 2] << 8 ) |
                 (RAM.data[i + 1] << 16 ) |
                 (RAM.data[i] << 24)
               );

}

// Funcao que converte um inteiro em 4 bytes/chars
void int_to_chars(int int_num, char *char_num)
{
    int i, aux;
    for(i = 3; i >= 0; i--)
    {
        char_num[i] = (char)(int_num & 0xFF);
        int_num >>= 8;
    }
}

void auxiliar_free() {
    int i;

    for ( i = 0; i < 9; i++ )
        free( Auxiliar.fmtStr[i] );
}

void cache_free() {
    int i, j, k;

    for ( i = 0; i < Cache.setNum; i++ ) {
        for ( j = 0; j < Cache.associativity; j++ )
            free( Cache.set[i].block[j].data );
        free( Cache.set[i].block );
    }
    free(Cache.set);
}

void ram_free() {
    free(RAM.data);
}
